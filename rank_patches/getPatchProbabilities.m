function [patchProb,boxes,clusterAssignment] = getPatchProbabilities(catIdx,imgIdx)
load('../word_document_prob.mat');
load('../word_document_prob2.mat');
load('../cluster_code/gaussian_parameters2.mat');
load('../selective_search_boxes.mat');

numClusters = size(means,2);
numBoxes = size(selective_search_boxes{catIdx}{imgIdx},1);

patchProb = zeros(numBoxes,1);

feature = getCRNNfeature2(catIdx,imgIdx);
[~,feat_dim] = size(feature);


likelihood_patch_given_word = zeros(numBoxes,numClusters);
for i=1:numClusters
    likelihood_patch_given_word(:,i) = mvnpdf(feature,means{1,i}, ...
                                              covariances{1,i});
end

for i=1:numBoxes
    likelihood_patch_given_word(i,:) = ...
        likelihood_patch_given_word(i,:)/sum(likelihood_patch_given_word(i,:));
end
    
[prob_patch_given_word,clusterAssignment] = max(likelihood_patch_given_word,[],2);

if(imgIdx<=50)
    prob_word_given_document = word_document_prob(clusterAssignment, ...
                                              (catIdx-1)*50+imgIdx);
end
if(imgIdx>50 && imgIdx<=75)
    prob_word_given_document = word_document_prob2(clusterAssignment, ...
                                              (catIdx-1)*25+imgIdx-50);
end
patchProb = prob_word_given_document.*prob_patch_given_word;

patchProb(isnan(patchProb))=0;

boxes = selective_search_boxes{catIdx}{imgIdx};

