function [crnn_feat] = getCRNNfeature(catidx,image_idx,b)

%get image name
if(~exist('../image_names'))
    load('../image_names.mat');
end
image_name=image_names{catidx}{image_idx};

%get box
if(~exist('../selective_search_boxes'))
    load('../selective_search_boxes.mat')
end
boxes=selective_search_boxes{catidx}{image_idx};
box=boxes(bb_idx,:);

%get crnn
crnn_name=strcat('../CRNN_features/',image_name);
crnn_name=strcat(crnn_name,'.caffe.txt.mat');
load(crnn_name);
crnn_feat=feat(bb_idx,:);

%reduce dim
if(~exist('eigvector'))
    load('../cluster_code/pca_kmeans_vars.mat')
end

crnn_feat=(crnn_feat - meanData)*eigvector;
end