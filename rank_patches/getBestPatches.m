function [best_boxes,best_clusterIds] = getBestPatches(catIdx,imgIdx,num_best)
[patchProb,boxes,clusterAssignment] = getPatchProbabilities(catIdx, ...
                                                  imgIdx);
[sorted_prob, sorted_idx] = sort(patchProb,'descend');
sorted_boxes = boxes(sorted_idx(:,1),:);
sorted_clusterIds = clusterAssignment(sorted_idx(:),:);
[~,best_ids,~] = unique(sorted_clusterIds,'stable');
best_boxes = sorted_boxes(best_ids(1:num_best),:);
best_clusterIds = sorted_clusterIds(best_ids(1:num_best));
end