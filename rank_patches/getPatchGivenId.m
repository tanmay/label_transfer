function [patch] = getPatchGivenId(id)

load('../image_names.mat');
load('../selective_search_boxes.mat');
flis = image_names{1}(1:50);
ct=1;
for i=1:1:length(flis)
    [n,~]=size(selective_search_boxes{1}{i});
    for j=1:1:n
        if ct == id
            im = imread(strcat('../Images/',flis{i}));
            box=selective_search_boxes{1}{i}(j,:);
            patch = im(box(1):box(3),box(2):box(4),:);
            return;
        end
        ct = ct + 1;
    end
end

flis = image_names{2}(1:50);
for i=1:1:length(flis)
    [n,~]=size(selective_search_boxes{2}{i});
    for j=1:1:n
        if ct == id
            im = imread(strcat('../Images/',flis{i}));
            box=selective_search_boxes{2}{i}(j,:);
            patch = im(box(1):box(3),box(2):box(4),:);
            return;
        end
        ct = ct + 1;
    end
end

patch= [];

end