load('../cluster_code/pca_gmm_vars.mat');
load('../cluster_code/gaussian_parameters2.mat');
MAX=10;

for i=1:1:500
    ll_patch_given_word = mvnpdf(new_data,means{1,i},covariances{1,i});
    size(ll_patch_given_word)
    [sortedValues,sortIndex] = sort(ll_patch_given_word(:),'descend');  %# Sort the values in                                            %#   descending order
    maxIndex = sortIndex(1:MAX);
    for j=1:1:MAX
        patch=getPatchGivenId(maxIndex(j));
        if~exist(strcat('patches/',int2str(i)))
            mkdir(strcat('patches/',int2str(i)));
        end
        fout=strcat(strcat(strcat(strcat('patches/',int2str(i)),'/'),int2str(j)),'.png');
        imwrite(patch,fout);
    end
end