function showBestBoxes(catIdx,imgIdx,num_best)
[best_boxes,best_clusterIds] = getBestPatches(catIdx,imgIdx, ...
                                                     num_best);
load('../image_names.mat');
load('../image_categories.mat');
filename = ['../Images/' ...
            '/' image_names{catIdx}{imgIdx}];
%filename = ['../LabelMe_Dataset/Images/' image_categories{catIdx} ...
%            '/' image_names{catIdx}{imgIdx}];
img = imread(filename);
%keyboard;
figure, imshow(img);
num_boxes = size(best_boxes,1);
for i=1:num_boxes
    rectangle('Position',[best_boxes(i,2) best_boxes(i,1) ...
                        best_boxes(i,4)-best_boxes(i,2) ...
                        best_boxes(i,3)-best_boxes(i,1)], 'LineWidth',1,'EdgeColor',[0 1 0]);
                    
end
best_clusterIds
end
    
    
    
