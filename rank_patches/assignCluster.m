function clusterIdx = assignCluster(feature,cluster_centers)
numClusters = size(cluster_centers,2);
dist2centers = zeros(1,numClusters);
% for i=1:numClusters
%     dist2centers(1,i) = norm(feature-cluster_centers{1,i},2);
% end
dist2center = pdist2(feature,cluster_centers);
[~,clusterIdx] = min(dist2centers);
end

    