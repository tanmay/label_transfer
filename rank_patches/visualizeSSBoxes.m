function visualizeSSBoxes(catIdx,imgIdx)

load('../image_names.mat');
load('../image_categories.mat');
filename = ['../Images/' ...
            '/' image_names{catIdx}{imgIdx}];

        N = 500;
img = imread(filename);
load('../selective_search_boxes.mat');

numBoxes = size(selective_search_boxes{catIdx}{imgIdx},1);
permutation = randperm(numBoxes,N);
figure, imshow(img);
for i=1:N
    box = selective_search_boxes{catIdx}{imgIdx}(permutation(1,i),:);
    rectangle('Position',[box(2) box(1) box(4)-box(2) box(3)-box(1)]);
end
end