load('pca_kmeans_vars.mat');

%save histograms in cell array
load('../image_names.mat');
flis = image_names{1}(1:50);
flis = [flis; image_names{2}(1:50)];
matlis={};
for i=1:1:length(flis)
    f=strcat(flis{i},'.caffe.txt.mat');
    f=strcat('../CRNN_features/',f);
    matlis{end+1}=f;
end

hists={};
ct=1;
for i=1:1:length(matlis)
    load(matlis{i});
    [n,~]=size(feat);
    prev=ct;
    ct = ct + n;
    vec=idx(prev:ct-1);
    hist=histc(vec,1:100);
    hists{end+1}=hist;
end

save('histograms','hists');

%generate gaussians for all points in cluster and save mean and covariance
%in .mat file
means={};
covariances={};
for i=1:1:100
   mat=new_data(find(idx==i),:);
   means{end+1}=mean(mat);
   covariances{end+1}=cov(mat);
end

save('gaussian_parameters','means','covariances');

