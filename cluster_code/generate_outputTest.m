load('pca_gmm_vars.mat');

%save histograms in cell array
load('../image_names.mat');
flis = image_names{1}(51:75);
flis = [flis; image_names{2}(51:75)];
matlis={};
for i=1:1:length(flis)
    f=strcat(flis{i},'.caffe.txt.mat');
    f=strcat('../CRNN_features/',f);
    matlis{end+1}=f;
end

hists={};
ct=1;
allFeats=[];
for i=1:1:length(matlis)
    load(matlis{i});
    [n,~]=size(feat);
    allFeats = [allFeats; feat];
end

[n,~]=size(allFeats);
allFeats=(allFeats - repmat(meanData,n,1))*eigvector;

idx = cluster(gm,allFeats);
for i=1:1:length(matlis)
    load(matlis{i});
    [n,~]=size(feat);
    prev=ct;
    ct = ct + n;
    vec=idx(prev:ct-1);
    hist=histc(vec,1:500);
    hists{end+1}=hist;
end

save('histogramsTest','hists');