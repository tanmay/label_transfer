load('pca_gmm_vars.mat');

%save histograms in cell array
load('../image_names.mat');
flis = image_names{1}(1:50);
flis = [flis; image_names{2}(1:50)];
matlis={};
for i=1:1:length(flis)
    f=strcat(flis{i},'.caffe.txt.mat');
    f=strcat('../CRNN_features/',f);
    matlis{end+1}=f;
end

hists={};
ct=1;
for i=1:1:length(matlis)
    load(matlis{i});
    [n,~]=size(feat);
    prev=ct;
    ct = ct + n;
    vec=idx(prev:ct-1);
    hist=histc(vec,1:500);
    hists{end+1}=hist;
end

save('histograms2','hists');

%generate gaussians for all points in cluster and save mean and covariance
%in .mat file
means={};
covariances={};
for i=1:1:500
    mean=gm.mu(i,:);
    means{end+1}=mean;
    covariances{end+1} = diag(gm.Sigma(:,:,i));
end

save('gaussian_parameters2.mat','means','covariances');

