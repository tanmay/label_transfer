%first 50 are train
%second 25 are test

PCA_SIZE=-1;
DIM=4096;

load('../image_names.mat');
flis = image_names{1}(1:50);
flis = [flis; image_names{2}(1:50)];
matlis={};
for i=1:1:length(flis)
%for i=1:1:20
    f=strcat(flis{i},'.caffe.txt.mat');
    f=strcat('../CRNN_features/',f);
    matlis{end+1}=f;
end

all_feats=[];
for i=1:1:length(matlis)
    load(matlis{i});
    all_feats=[all_feats; feat];
end

%all_feats = all_feats(randperm(size(all_feats,1)),:);

%select first PCA_SIZE for PCA
if(PCA_SIZE==-1)
   PCA_SIZE=size(all_feats,1);
end
pca_data = all_feats(1:PCA_SIZE,:);

options.ReducedDim=DIM;
[eigvector, eigvalue, meanData, new_data] = PCA(double(pca_data), options);

save('eigvalue','eigvalue');
