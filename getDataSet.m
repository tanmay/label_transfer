%% Add LabelMeToolbox to path
addpath(genpath('LabelMeToolbox'));
HOMEIMAGES = './LabelMe_Dataset/Images';
HOMEANNOTATIONS = './LabelMe_Dataset/Annotations';
folderlist = {'bedroom_indoor_256x256_static','barcelona_static_street'};
LMinstall (folderlist, HOMEIMAGES, HOMEANNOTATIONS);