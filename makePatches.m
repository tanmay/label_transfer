addpath('cluster_code');
load('cluster_code/pca_kmeans_vars.mat');

load('image_names.mat');
load('selective_search_boxes');
flis = image_names{1}(1:50);
ct = 1;
for i=1:1:50
    im=imread(strcat('Images/',flis{i}));
    boxes=selective_search_boxes{1}{i};
    for j=1:1:length(boxes)
        label=idx(ct);
        ct = ct + 1;
        box=boxes(j,:);
        patch=im(box(1):box(3),box(2):box(4),:);
        if~exist(strcat('patches/',int2str(label)))
            mkdir(strcat('patches/',int2str(label)));
        end
        fout=strcat(strcat(strcat(strcat('patches/',int2str(label)),'/'),int2str(ct)),'.png');
        imwrite(patch,fout);
    end
end

flis = image_names{2}(1:50);
for i=1:1:50
    im=imread(strcat('Images/',flis{i}));
    boxes=selective_search_boxes{2}{i};
    for j=1:1:length(boxes)
        label=idx(ct);
        ct = ct + 1;
        box=boxes(j,:);
        patch=im(box(1):box(3),box(2):box(4),:);
        if~exist(strcat('patches/',int2str(label)))
            mkdir(strcat('patches/',int2str(label)));
        end
        fout=strcat(strcat(strcat(strcat('patches/',int2str(label)),'/'),int2str(ct)),'.png');
        imwrite(patch,fout);
    end
end