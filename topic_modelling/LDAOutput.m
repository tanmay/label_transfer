Term_dist = dlmread('term_distribution.txt');
Topic_dist = dlmread('topic_distribution.txt');

Term_dist = diag(1./sum(Term_dist,2))*Term_dist;
Topic_dist = diag(1./sum(Topic_dist,2))*Topic_dist;

Posterior_dist = Term_dist'*Topic_dist';
