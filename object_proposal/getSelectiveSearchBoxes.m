load('../image_categories.mat');
load('../image_names.mat');

IMAGE_DIR='../../LabelMe_Dataset/Images';

cd 'selective_search_ijcv_with_python';

num_categories = numel(image_categories);
selective_search_boxes = cell(num_categories,1);

for i=1:num_categories
    num_images = size(image_names{i},1);
    image_filename = cell(num_images,1);
    for j=1:num_images
        image_filename{j,1} = fullfile(IMAGE_DIR, ...
                          image_categories{i}, image_names{i}{j});
    end
    selective_search_boxes{i,1} = selective_search_rcnn(image_filename);
end

cd ..

save('../selective_search_boxes.mat','selective_search_boxes');