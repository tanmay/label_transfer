% Get the image categories and filenames for all images in the
% dataset
% Output - 
% image_categories.mat {'image_categories'} - a cell array of
% image categories
% image_names.mat {'image_names'} - a cell array of image file names where
% the ith cell array contains a cell array of names of image files
% in that category   
 
IMAGE_DIR = '../LabelMe_Dataset/Images';
folder_contents = dir(IMAGE_DIR);
directories = [folder_contents(:).isdir];
image_categories = {folder_contents(directories).name}';
image_categories(ismember(image_categories,{'.','..'})) = [];

save('../image_categories.mat','image_categories');

num_categories = size(image_categories,1);
image_names = cell(num_categories,1);
for i=1:num_categories
    CATEGORY_DIR = [IMAGE_DIR '/' image_categories{i}];
    image_names{i,1} = dir(fullfile(CATEGORY_DIR,['*.jpg']));
    image_names{i,1} = {image_names{i,1}.name}';
end

save('../image_names.mat','image_names');