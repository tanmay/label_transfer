import socket
import sys
from threading import Thread
from struct import pack
import time
import os.path
from ioutils import send_msg, recv_msg


def sender(HOST, PORT, input_fname, output_fname):        
    s         = socket.socket()         # Create a socket object
    host      = socket.gethostname() # Get local machine name    
    s.connect((HOST, PORT))
    
    # receive initial connection message
    message = recv_msg(s)
    print message

    # send data for computation
    with open(input_fname, 'rb') as fin:
        data = fin.read()
        print data
        send_msg(s, data)

    # receive waiting
    #for computation message
    print 'waiting'
    result = recv_msg(s)

    print 'writing results!'
    # get results
    fout = open(output_fname, 'wb')
    fout.write(result)
    s.close()

# sender() ends


if __name__ == '__main__':

    HOST       = 'ali.cs.uiuc.edu'             # symbolic name meaning all available interfaces
    PORT       = 8888           # arbitrary non-privileged port
    input_fname = sys.argv[1]
    output_fname = sys.argv[2] # end this with a .mat
    sender(HOST, PORT, input_fname, output_fname)

