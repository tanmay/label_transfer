load('image_names');
load('selective_search_boxes.mat');

type=1;

for i=1:1:length(image_names{type})
   image_names{type}(i);
   s=image_names{type}(i);
   fname = strcat(s{1},'.caffe.txt');
   fname = strcat('caffe_input/',fname);
   fout = fopen(fname,'w');
   fprintf(fout,'%s\n',s{1});
   mat = selective_search_boxes{type}{i};
   [n, ~] = size(mat);
   for j=1:1:n
      fprintf(fout,'%d %d %d %d\n',mat(j,2),mat(j,1),mat(j,4),mat(j,3));
   end
   fclose(fout);
end